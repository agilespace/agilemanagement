﻿using AgileTraining.Datos;
using AgileTraining.Model.Entities.Personas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileTraining.Services.Persons
{
    public static class PersonServices
    {

        public static List<Person> PersonsList(string status)
        {

            try
            {
                using (var dc = new AgileTrainingEntities())
                {

                    var pList = dc.Persons.Where(d => d.Status == status)
                                  .Select(p => new Person
                                  {
                                    IdCompany = p.IdCompany,
                                    Id = p.Id,
                                    Name = p.Name,
                                    MiddleName = p.MiddleName,
                                    LastName = p.LastName,
                                    SecondLastName = p.SecondLastName,
                                    Gender = p.Gender,
                                    IdArea = p.IdArea,
                                    IdRole = p.IdRole,
                                    Phone = p.Phone,
                                    CellPhone = p.CellPhone,
                                    Mail = p.Mail,
                                    Created = p.Created,
                                    IdUser = p.IdUser,
                                    Status = p.Status
                                  }).ToList();

                    return pList;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }


    }
}
