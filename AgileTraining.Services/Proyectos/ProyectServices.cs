﻿using AgileTraining.Datos;
using AgileTraining.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileTraining.Services.Proyectos
{
    public static class ProyectServices
    {
        public static List<Proyect> LoadCurrentProyects(string status)
        {

            try
            {
                using (var dc = new AgileTrainingEntities())
                {

                    var pList = dc.AgileProyects.Where(x => x.Status == status)
                                  .Select(p => new Proyect
                                  {
                                      IdCompany = p.IdCompany,
                                      Id = p.Id,
                                      Name = p.Name,
                                      Description = p.Description,
                                      CoachAssigned = p.CoachAssigned,
                                      CoachName = p.UsersCoachs.Persons.Name,
                                      DGA = p.DGA,
                                      DGAName = dc.DGA.FirstOrDefault(x => x.IdCompany ==p.IdCompany && x.Id == p.DGA ).Name,
                                      Area = p.Area,
                                      AreaName = dc.Area.FirstOrDefault(x => x.IdCompany == p.IdCompany && x.Id == p.Area).Name,
                                      StartDate = p.StartDate,
                                      EndTime = p.EndTime,
                                      RegisterDate = p.RegisterDate,
                                      Presupuestado = p.Presupuestado,
                                      Comments = p.Comments,
                                      IdUser = p.IdUser,
                                      Status = p.Status
                                  }).ToList();

                    return pList;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }


    }
}
