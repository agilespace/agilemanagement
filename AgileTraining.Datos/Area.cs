//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AgileTraining.Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class Area
    {
        public int IdCompany { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Manager { get; set; }
        public int IdDGA { get; set; }
        public string Status { get; set; }
    
        public virtual StatusCatalogs StatusCatalogs { get; set; }
        public virtual Company Company { get; set; }
        public virtual DGA DGA { get; set; }
    }
}
