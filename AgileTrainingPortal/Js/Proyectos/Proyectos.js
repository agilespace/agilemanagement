﻿$(document).ready(function () {

    LoadProyectList();


})



function LoadProyectList() {

    $('#SectionList').load('Proyects/List', function () {
        InitTable();
//        $(".preloader-wrapper.small").removeClass("active");
    });

}


function InitTable() {

    $('#tblProyects').dataTable(
        {
            language: {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "No se encontraron resultados.",
                "info": "Página _PAGE_ de _PAGES_",
                "sInfo": "Registros de _START_ al _END_ (_TOTAL_)",
                "sInfoEmpty": "Página _PAGE_ de _PAGES_",
                //"sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            responsive: true,
            fixedHeader: true,
            searching: true,
            "bSort": true,
            search: {
                smart: true
            },
            "bAutoWidth": false,
            "bLengthChange": false,
            "bPaginate": false,
            "pageLength": 1000,
            destroy: true,
            "info": true,
            "bInfo": true

        }
    );

    

    //$("#tblTablaAlumnos_filter").css({ "float": "left", "text-align": "left" });

}