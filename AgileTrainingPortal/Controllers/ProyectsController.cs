﻿using AgileTraining.Services.Proyectos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;


namespace AgileTraining.Web.Controllers
{
    public class ProyectsController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List(string status = "A")
        {

            var res = ProyectServices.LoadCurrentProyects(status);

            return PartialView("List", res);

        }

    }
}
