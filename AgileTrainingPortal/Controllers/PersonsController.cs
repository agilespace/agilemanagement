﻿using AgileTraining.Services.Persons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace AgileTraining.Web.Controllers
{
    public class PersonsController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult List()
        {

            var res = PersonServices.PersonsList("A");

            return Json(res, JsonRequestBehavior.AllowGet);

        }

    }
}
