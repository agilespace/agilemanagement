﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileTraining.Model.Entities
{
    public class Proyect
    {
        DateTime dt;
        TimeSpan tm;
        string _presupuesto;
        public int IdCompany { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int CoachAssigned { get; set; }
        public string CoachName { get; set; }
        public int DGA { get; set; }
        public string DGAName { get; set; }
        public int Area { get; set; }
        public string AreaName { get; set; }
        public DateTime? StartDate { get; set; }
        public string sStartDate {
            get
            {
                return StartDate.HasValue ? String.Format("{0:dd/MM/yyyy}", StartDate) : "";
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    DateTime.TryParse(value, out dt);
                    StartDate = dt;
                }
            }
        }
        public DateTime? EndTime { get; set; }
        public string sEndTime {
            get
            {
                return EndTime.HasValue ? String.Format("{0:dd/MM/yyyy}", EndTime) : "";
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    DateTime.TryParse(value, out dt);
                    EndTime = dt;
                }
            }
        }
        public DateTime? RegisterDate { get; set; }
        public string sRegisterDate {
            get
            {
                return RegisterDate.HasValue ? String.Format("{0:dd/MM/yyyy}", RegisterDate) : "";
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    DateTime.TryParse(value, out dt);
                    RegisterDate = dt;
                }
            }
        }
        public bool? Presupuestado { get; set; }
        public string sPresupuesto {
            get
            {
                return Presupuestado == true ? "Si" : "No";
            }
            set
            {
                _presupuesto = value;
            }

        }
        public string Comments { get; set; }
        public int IdUser { get; set; }
        public string Status { get; set; }              

    }

}
