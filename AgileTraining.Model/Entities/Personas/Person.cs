﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileTraining.Model.Entities.Personas
{
    public class Person
    {

        public int IdCompany { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string SecondLastName { get; set; }
        public string Gender { get; set; }
        public int IdArea { get; set; }
        public int IdRole { get; set; }
        public string Phone { get; set; }
        public string CellPhone { get; set; }
        public string Mail { get; set; }
        public DateTime? Created { get; set; }
        public int IdUser { get; set; }
        public string Status { get; set; }




    }
}
